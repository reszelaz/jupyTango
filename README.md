## About jupyTango & bokeh
A control system client requires both static and dynamic data visualization (i.e plots and live/asynchronous monitors). Since Jupyter doesn't provide any 'official' data visualization solution, we need to select one. Among the available solutions, [bokeh](http://bokeh.pydata.org/en/latest) presents the highest potential for our application. This is mainly due to the fact that [Bokeh application model relies on the Tornado web server](https://docs.bokeh.org/en/latest/docs/dev_guide/server.html) who provides the fundamentals for our asynchronous implementation. See 'jupytango/session.py' for details about jupyTango asynchronous features.

## Installing jupyTango with conda under Ubuntu linux
Here is a step by step jupyTango installation procedure under .

#### Step-00: (if required) follow the tango installation procedure for [ubuntu](https://tango-controls.readthedocs.io/en/latest/installation/tango-on-linux.html#debian-ubuntu)
- `sudo apt install mariadb-server`
- `sudo apt install tango-db tango-test`

#### Step-01: install [anaconda](https://www.anaconda.com/products/individual)
- install [anaconda (python >= 3.7)](https://www.anaconda.com/products/individual)
- create a dedicated jupytango environment (optional but recommended): `conda create -n jupytango python=3.7`
- activate the jupytango environment: `conda activate jupytango`

#### Step-02: install the jupyTango dependencies
- `conda install -c conda-forge opencv jupyterlab ipywidgets jupyter_bokeh pytango itango`

#### Step-03: create the jupyTango profile (i.e. [ipython](https://ipython.org) profile) 
- `cp -Rf $HOME/.ipython/profile_default $HOME/.ipython/profile_jupytango`
- `echo "config = get_config()" > $HOME/.ipython/profile_jupytango/ipython_config.py`
- `echo "config.InteractiveShellApp.extensions = ['jupytango']" >> $HOME/.ipython/profile_jupytango/ipython_config.py`

#### Step-04: create the jupyTango kernel (i.e. the [jupyter](https://jupyter.org) kernel) 
- `python -m ipykernel install --user --name jupyTango --display-name "jupyTango" --profile jupytango`

#### Step-05: copy kernel logo into the jupyTango kernel direcrtory
- `cp ./resources/logo/* $HOME/.local/share/jupyter/kernels/jupytango`

### Opening a jupyTango notebook
- Open a terminal 
- Be sure that jupyTango is in your PYTHONPATH: e.g. `export PYTHONPATH=<YOUR-PATH-TO-JUPYTANGO>`
- Specify the jupyTango context by typing: `export JUPYTER_CONTEXT=LAB` (this will be removed in a near future)
- Start jupyterlab by typing: `jupyter-lab` (will spawn a web browser instance)
- From the `Launcher` tab of the web browser `JupyterLab` tab, open a `jupyTango` notebook (click on the Tango logo)
- The `01_introduction.ipynb` notebook is a good entry point to get started with jupyTango 
- The provided examples use the classical TangoTest device 'sys/tg_test/1'


## Giving jupyTango a try using Docker

The following assumes that docker is properly installed on your system.

#### Build the Docker image 
`cd ./docker`

`sudo docker build -t jupytango:1.0.0 .`

#### Create the jupytango-net network (for communication between Docker containers) 
`sudo docker network create jupytango-net`

#### Launch the jupyTango container
`sudo docker-compose up`

#### Connect to Jupyterlab

Search for something like "http://127.0.0.1:8888/lab?token=..." in the docker-compose log and open a browser to the specified URL.

#### Be sure the TangoTest requirement is satisfied

The jupyTango examples use the classical TangoTest device 'sys/tg_test/1. Be sure it is running in your control system.

#### Enjoy the provided examples 

The provided `01_introduction.ipynb` notebook is a good entry point to get started with jupyTango.



